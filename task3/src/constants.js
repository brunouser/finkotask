export const fontApiUrl = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyC6ngJxGNLXOiUHmJT70u3s7aUQ8fOmBI4";

export const labels = {
  appTitle: 'Select a google font',
  availableFonts: 'Available fonts',
  sampleText: 'Sample text',
  sampleValue: 'The quick brown fox jumped over the lazy dog.'
};
